#define RCC_CR (*(volatile unsigned int*)0x40021000)
#define RCC_CFGR (*(volatile unsigned int*)0x40021004) 
#define RCC_AHBENR (*(volatile unsigned int*)0x40021014)
#define RCC_APB2ENR (*(volatile unsigned int*)0x40021018)
#define RCC_APB1ENR (*(volatile unsigned int*)0x4002101C) /*CLK macro*/

#define FLASH_ACR (*(volatile unsigned int*)0x40022000) /*flash*/

#define GPIOA_CRL (*(volatile unsigned int*)0x40010800)
#define GPIOA_CRH (*(volatile unsigned int*)0x40010804)
#define GPIOA_IDR (*(volatile unsigned int*)0x40010808)
#define GPIOA_ODR (*(volatile unsigned int*)0x4001080C)
#define GPIOA_BSRR (*(volatile unsigned int*)0x40010810)
#define GPIOA_BRR (*(volatile unsigned int*)0x40010814)
#define GPIOA_LCKR (*(volatile unsigned int*)0x40010818) /*A*/
             
#define GPIOB_CRL (*(volatile unsigned int*)0x40010C00)
#define GPIOB_CRH (*(volatile unsigned int*)0x40010C04)
#define GPIOB_IDR (*(volatile unsigned int*)0x40010C08)
#define GPIOB_ODR (*(volatile unsigned int*)0x40010C0C)
#define GPIOB_BSRR (*(volatile unsigned int*)0x40010C10)
#define GPIOB_BRR (*(volatile unsigned int*)0x40010C14)
#define GPIOB_LCKR (*(volatile unsigned int*)0x40010C18) /*B*/
            
#define GPIOC_CRL (*(volatile unsigned int*)0x40011000)
#define GPIOC_CRH (*(volatile unsigned int*)0x40011004)
#define GPIOC_IDR (*(volatile unsigned int*)0x40011008)
#define GPIOC_ODR (*(volatile unsigned int*)0x4001100C)
#define GPIOC_BSRR (*(volatile unsigned int*)0x40011010)
#define GPIOC_BRR (*(volatile unsigned int*)0x40011014)
#define GPIOC_LCKR (*(volatile unsigned int*)0x40011018) /*C*/
            
#define GPIOD_CRL (*(volatile unsigned int*)0x40011400)
#define GPIOD_CRH (*(volatile unsigned int*)0x40011404)
#define GPIOD_IDR (*(volatile unsigned int*)0x40011408)
#define GPIOD_ODR (*(volatile unsigned int*)0x4001140C)
#define GPIOD_BSRR (*(volatile unsigned int*)0x40011410)
#define GPIOD_BRR (*(volatile unsigned int*)0x40011414)
#define GPIOD_LCKR (*(volatile unsigned int*)0x40011418) /*GPIO D*/
            
#define TIM2_CR1 (*(volatile unsigned int*)0x40000000)
#define TIM2_CR2 (*(volatile unsigned int*)0x40000004)
#define TIM2_SR (*(volatile unsigned int*)0x40000010)
#define TIM2_CNT (*(volatile unsigned int*)0x40000024)
#define TIM2_PSC (*(volatile unsigned int*)0x40000028)
#define TIM2_ARR (*(volatile unsigned int*)0x4000002C) /*Timer2*/

#define TIM3_CR1 (*(volatile unsigned int*)0x40000400)
#define TIM3_CR2 (*(volatile unsigned int*)0x40000404)
#define TIM3_SR (*(volatile unsigned int*)0x40000410)
#define TIM3_CNT (*(volatile unsigned int*)0x40000424)
#define TIM3_PSC (*(volatile unsigned int*)0x40000428)
#define TIM3_ARR (*(volatile unsigned int*)0x4000042C) /*Timer3*/

#define USART_SR (*(volatile unsigned int*)0x40013800)
#define USART_DR (*(volatile unsigned int*)0x40013804)
#define USART_BRR (*(volatile unsigned int*)0x40013808)
#define USART_CR1 (*(volatile unsigned int*)0x4001380C)
#define USART_CR2 (*(volatile unsigned int*)0x40013810)
#define USART_CR3 (*(volatile unsigned int*)0x40013814)
#define USART_GTPR (*(volatile unsigned int*)0x40013818) /*USART1*/

#define ADC1_SR (*(volatile unsigned int*)0x40012400)
#define ADC1_CR1 (*(volatile unsigned int*)0x40012404)
#define ADC1_CR2 (*(volatile unsigned int*)0x40012408)
#define ADC1_SMPR1 (*(volatile unsigned int*)0x4001240C)
#define ADC1_SMPR2 (*(volatile unsigned int*)0x40012410)
#define ADC1_SQR1 (*(volatile unsigned int*)0x4001242C)
#define ADC1_SQR2 (*(volatile unsigned int*)0x40012430)
#define ADC1_SQR3 (*(volatile unsigned int*)0x40012434)
#define ADC1_DR (*(volatile unsigned int*)0x4001244C) /*ADC1*/

#define DMA_CCR1 (*(volatile unsigned int*)0x40020008)
#define DMA_CNDTR1 (*(volatile unsigned int*)0x4002000C)
#define DMA_CPAR1 (*(volatile unsigned int*)0x40020010)
#define DMA_CMAR1 (*(volatile unsigned int**)0x40020014) /*DMA*/

#define NVIC 0xE000E100
#define NVIC_ISER1 (*(volatile unsigned int*)0xE000E104)
