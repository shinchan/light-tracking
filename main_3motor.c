#include <math.h>
#include "registers.h"
#include "functions.h"
#define STD_STEP 16
#define QUARTER_STDSTEP STD_STEP/4
#define MOTOR_NUM 3
#define M_PI 3.14159265358979323846264338327
#define STEP_ANGLE 5.625/64*M_PI/180*61/64
/*
MOTOR_NUM dependent function:
ADC_fetch3
calc_coordinate
motor_rotate3
motor_stop
*/

/*static unsigned int calibration_data[MOTOR_NUM][3]={0};*/ 
static unsigned int calibration_diff[MOTOR_NUM]={0};
static unsigned int DMA_addr[3*MOTOR_NUM];
static unsigned int* DMA_source_ptr=DMA_addr;
static volatile int ctrl_int;
static volatile int status_int; /* 0:in main 1:in tracing mode */
static char rd[7];
static int ptr=0;
static int already_init_ADC1=0;
static int current_motor_phase[MOTOR_NUM]={0};
static char coordinate_str[33];
static char* cstrp=coordinate_str;
static int current_step[MOTOR_NUM]={0};
struct
{
 int x,y; 
}static coordinate;

/*void calc_coordinate()
{
  int i;
  float angle[MOTOR_NUM],step_angle=5.625/180*M_PI*2,a=100,temp;
  for(i=0;i<MOTOR_NUM;i++)
    angle[i]=(float)current_step[i]*step_angle;
  temp=a*sin(angle[1]/sin(M_PI/3+angle[1])-angle[2]);
  coordinate.x=(int)((a*sin(M_PI/3-angle[1])/sin(M_PI/3+angle[0])+sqrt(a*a+temp*temp-2*a*temp*cos(angle[2]))+a*sin(angle[2])/sin(M_PI/3+angle[2]-angle[0]))/3);
  coordinate.y=(int)(((60-angle[2]*180/M_PI)*2+180/M_PI*(M_PI/2-atan((a/sqrt(a*a+temp*temp-2*a*temp*cos(angle[2]))-cos(angle[2]))/sin(angle[2]))))/3);
}*/
/*
void calc_coordinate()
{
	float angle0=STEP_ANGLE*current_step[0],angle2=STEP_ANGLE*current_step[2],temp,test;
	temp=130.0*tan(angle2)/(tan(angle2)-tan(angle0));
	coordinate.x=(test=temp-65)<0?(int)(test-0.5):(int)(test+0.5);
	coordinate.y=(test=-temp*tan(angle0))<0?(int)(test-0.5):(int)(test+0.5);
}
*/
void calc_coordinate()
{
	float angle0=STEP_ANGLE*current_step[0],angle2=STEP_ANGLE*current_step[2],temp,test,a=100,h;
        temp=a/(tan(M_PI/3-angle0)/tan(angle2)+1);
        h=temp*tan(M_PI/3-angle0);
	coordinate.x=(test=sqrt(temp*temp+h*h))<0?(int)(test-0.5):(int)(test+0.5);
       	coordinate.y=(test=angle0*180/M_PI)<0?(int)(test-0.5):(int)(test+0.5); 
}
/*
void calc_coordinate()
{
	float angle0=STEP_ANGLE*current_step[0],angle1=STEP_ANGLE*current_step[1],angle2=STEP_ANGLE*current_step[2],temp,test,a=200,h;
        if(absolute(angle0+angle2)<M_PI/6&&absolute(angle0+angle1)>M_PI/6)
           angle2=M_PI/3-angle1;
        temp=a/(tan(M_PI/3-angle0)/tan(angle2)+1);
        h=temp*tan(M_PI/3-angle0);
	coordinate.x=(test=sqrt(temp*temp+h*h))<0?(int)(test-0.5):(int)(test+0.5);
       	coordinate.y=(test=angle0)<0?(int)(test-0.5):(int)(test+0.5); 
}
*/
char* inttoascii(int a,char* q)
{
  char p[33];
  int i=0,u=absolute(a),m=0;
  do
  {
    p[i++]=u%10+48;
    u/=10;
  }while(u);
  if(a<(u=absolute(a)))
  q[m++]='-';
  while(i)
  q[m++]=p[--i];
  q[m]=0;
  return q;
}
void print_coordinate()
{
  calc_coordinate();
  USART_transmit(inttoascii(coordinate.x,cstrp));
  USART_transmit("\r");
  USART_transmit(inttoascii(coordinate.y,cstrp));
  USART_transmit("\r");
}
void record_step(char* dir,int* step)
{
	int m;
	for(m=0;m<MOTOR_NUM;m++)
	{
		if(dir[m]=='C')
			current_step[m]+=step[m];
		else 
			current_step[m]-=step[m];
	}
} 
void roll_back(char* ctrl_direction,int* ctrl_step)
{
	int m;
	for(m=0;m<MOTOR_NUM;m++)
	{
		ctrl_step[m]=absolute(current_step[m]);
		if(current_step[m]>0)
			ctrl_direction[m]='A';
		else
			ctrl_direction[m]='C';
	}
	motor_rotate3(ctrl_direction,ctrl_step);
	record_step(ctrl_direction,ctrl_step);
} 
void enable_USART1_NVIC()
{
  NVIC_ISER1|=0x00000020;
}
void init_sysclk()
{
  RCC_CR|=0x00010000; /*set HSEON*/
  FLASH_ACR|=0x00000002; /*set flash_latency*/
  RCC_CFGR|=0x001D0000; /*set PLLMUL,PLLSRC*/
  RCC_CR|=0x01000000; /*set PLLON*/
  RCC_CFGR=(RCC_CFGR&0xFFFFFFFE)|0x00000002; /*set SW*/
  while((RCC_CFGR&0x0000000C)!=0x00000008);
  RCC_CFGR|=0x0000C600; /*USB/1.5,APB1/8,ADC/8*/
  RCC_AHBENR|=0x00000001; /*DMA1 clock enabled*/
  RCC_APB1ENR|=0x00000001; /*enable timer2*/
  RCC_APB2ENR|=0x0000467D; /*enable USART1,ADC1,2,portA,B,C,D,E and AFIO*/
  return;
}

void init_GPIO()
{
GPIOA_CRL=0x40000004;
GPIOA_CRH=0x488444A4;

/*GPIOB_CRL=0x44444444;*/
GPIOB_CRH=0x22222222;

GPIOC_CRL=0x44444000;
GPIOC_CRH=0x22224444;
return;

}

void set_timer2()
{
  TIM2_ARR=0x000007D0; /*auto-reload value*/ /*the value is 7D0 when the interval is 1s*/
  TIM2_PSC=0x00002327; /*prescaler=2^11*/
  TIM2_CR1|=0x0095; /*enable counter,UEV,auto-reload,downcount*/
}
void init_USART1()
{
  USART_BRR|=0x1D4C; /*set baud rate 9.6kbps*/
  USART_CR1|=0x202C; /*enable USART,8 data bit,disable TXE interrupt,enable RXNE interrupt,enable transmitter/receiver*/
  USART_CR2|=0x2000; /*two stop bit*/
  enable_USART1_NVIC();
}
void init_DMA()
{
  DMA_CMAR1=DMA_source_ptr;
  DMA_CPAR1=0x4001244C; /*source peripheral address*/
  DMA_CCR1=0x00000A80; /*memory size=peripheral size=32,memory increment enabled,circular mode disabled*/
}
void DMA_on()
{
  DMA_CNDTR1=0x0009;    /*3*MOTOR_NUM*/
  DMA_CCR1|=0x00000001; /*DMA channel enabled*/
}
void DMA_off()
{
  DMA_CCR1&=0xFFFFFFFE; /*DMA channel disabled,in order to reload DMA_CNDTR1*/
}
void init_ADC1()
{
  if(already_init_ADC1==1)
    return;
  init_DMA();
  ADC1_CR1|=0x00000100; /*scan mode*/
  ADC1_CR2|=0x000D0105; /*SWSTART as externel trigger,enable calibration,DMA,single conversion,set ADON*/
  ADC1_SMPR2|=0x00049248; /*sample time:7.5 cycles for channel 1,2,3,4,5,6,10,11,12*/
  ADC1_SQR1|=0x00800000;/*total number of channels to be scanned*/
  ADC1_SQR2|=0x00002D8A;/*sample sequence:1,3,2,4,6,5,10,12,11*/
  ADC1_SQR3|=0x0A420861;
  already_init_ADC1=1;
}


/*fetch three ADC value in consecutive memory starting from DMA_addr to memory starting from start_addr*/
int absolute(int a)
{
  if(a<0)
    return -a;
  else 
    return a;
}

void ADC_fetch3(unsigned int(*start_addr)[3]) 
{
  int temp[3][4*MOTOR_NUM],a;
  int i,j,k,m;
  for(j=0;j<=2;j++)
  {
  DMA_on();
  ADC1_CR2|=0x004000000; /*set SWSART(externel trigger)*/
  while(DMA_CNDTR1>0);/*wait until DMA transfer completes*/
  DMA_off();
  for(m=0;m<MOTOR_NUM;m++)
  {
    for(i=4*m,k=3*m;i<4*m+3;i++,k++)
      temp[j][i]=(int)(DMA_addr[k]&0x00000FFF);
    temp[j][i]=0;
  }
  }
  for(m=0;m<MOTOR_NUM;m++)
  {
    a=0;
    for(j=0;j<3;j++)
    {
      temp[j][4*m+3]=(temp[j][4*m]+temp[j][4*m+1]+temp[j][4*m+2])/3;
      a+=temp[j][3];
    }
    a/=3;
    k=0;
    for(j=0;j<3;j++)
    {
      if(absolute(temp[j][4*m+3]-a/3)<absolute(temp[k][4*m+3]-a/3))
        k=j;
    }
    for(i=0;i<3;i++)
      start_addr[m][i]=temp[k][4*m+i];
  }
  return;
}
void USART1_IRQHandler()
{
  RXNE_interrupt();
}
void RXNE_interrupt() /*return value: 1*/
{
  unsigned char t=USART_DR&0x000000FF;
  if((t!='\r')&&(t!='\n'))
    if(ptr<=5)
    {  
      rd[ptr++]=t;
      /*USART_transmit("rd");*/
    }
    else
    {
      ptr=0;
      USART_transmit("\rInvalid input:Buffer Overflow!\r");
    }
  else
  {
    rd[ptr]='\0'; 
    
    ptr=0;
    USART_process_rd3(rd);
    USART_CR1|=0x0020; /*re-enable RXNE interrupt*/
    USART_transmit("done sir!\r\n");
  }
}

void USART_process_rd3(char* rd)
{
  USART_CR1&=0xFFFFFFDF; /*disable RXNE interrupt,re-enabled after return to RXNE_interrupt()*/
  int i,j,step=0;
  int ctrl_step[MOTOR_NUM]={0};
  char ctrl_direction[MOTOR_NUM]={'C'};
  switch(rd[0])
  {
   case 'P':
      {
        if(!status_int)
        {
          USART_transmit("\rInvalid input:Not in tracing mode!\r");
          return;
        }
        else
        print_coordinate();
        return;
      }
   case 'E':
      {
        if(status_int)
          ctrl_int=0;
        else
           USART_transmit("\rInvalid input:Not in tracing mode!\r");
        return;
      }
    case 'S':
      {
        if(status_int)
          USART_transmit("\rInvalid input:Already in tracing mode!\r");
        else
          ctrl_int=1;
        return;
      }
    case 'B':
    	{
          if(status_int)
          {
            USART_transmit("\rInvalid input:Can't roll back now!\r");
            return;
          }
          else
            roll_back(ctrl_direction,ctrl_step);
          return;    	
    	}
    default:break;
  }
  switch(rd[1])
  {  
    case 'R':
      {
      	rd[0]-=49;
      	if(rd[0]>=MOTOR_NUM) /*if(rd[0]>=MOTOR_NUM|rd[0]<0)*/
        {
            USART_transmit("\rInvalid input!\r");
            return;
        }
      	Record_calibration(rd[0]);
      	return;
      }
    case 'C':;
    case 'A':
      {
        for(i=2;rd[i]!='\0';i++);
        for(j=1,i--;i>=2;i--,j*=10)
          if(rd[i]>57||rd[i]<48) /*parameter not between '0' and '9'*/
          {
            USART_transmit("\rInvalid input!\r");
            return;
          }
          else
            step+=j*(rd[i]-48);
          /***/  
          rd[0]-=49;
          if(rd[0]>=MOTOR_NUM) /*if(rd[0]>=MOTOR_NUM|rd[0]<0)*/
          {
            USART_transmit("\rInvalid input!\r");
            return;
          }
          for(i=0;i<MOTOR_NUM;i++)
            ctrl_step[i]=0;
          ctrl_step[rd[0]]=step;
          ctrl_direction[rd[0]]=rd[1];
          motor_rotate3(ctrl_direction,ctrl_step);
          break;
      }
    default:
      {
        USART_transmit("\rInvalid input!\r");
        return;
      }
  }
}

void motor_delay()
{
  TIM2_CNT=0x00000006;
  TIM2_SR&=0x0000FFFE;
  while((TIM2_SR&0x00000001)==0x00000000);
}

void phase_step(char direction,int i)
{
  switch(direction)
  {
    case 'C':   current_motor_phase[i]=(current_motor_phase[i]+1)%8;break;
    case 'A':   current_motor_phase[i]-=1;
                if(current_motor_phase[i]<0)
                current_motor_phase[i]+=8;
                break;
    default:    USART_transmit("Internal Error@phase_step Occurred!\r");
    			break;
  }
}
void motor_rotate3(char* direction,int* step)
{
  static unsigned GPIO_phase1[8]={0x0100,0x0300,0x0200,0x0600,0x0400,0x0C00,0x0800,0x0900};
  static unsigned GPIO_phase2[8]={0x1000,0x3000,0x2000,0x6000,0x4000,0xC000,0x8000,0x9000};
  static unsigned GPIO_phase3[8]={0x1000,0x3000,0x2000,0x6000,0x4000,0xC000,0x8000,0x9000};
  int t[MOTOR_NUM]={0},ctrl[MOTOR_NUM]={0},i,test=0;
  motor_delay();
  for(i=0;i<MOTOR_NUM;i++)
  {
    if(t[i]<step[i])
      ctrl[i]=1;
    else
      ctrl[i]=0;
    test|=ctrl[i];
    t[i]++;
  }
  while(test!=0)
  {
  	test=0;
    for(i=0;i<MOTOR_NUM;i++)
    {
      if(ctrl[i])
      {
        phase_step(direction[i],i);
		switch(i)
		{
			case 0:  GPIOB_ODR=((GPIOB_ODR&0xF0FF)|GPIO_phase1[current_motor_phase[i]]);
			         break;
			case 1:  GPIOB_ODR=((GPIOB_ODR&0x0FFF)|GPIO_phase2[current_motor_phase[i]]);
			         break;
			case 2:  GPIOC_ODR=((GPIOC_ODR&0x0FFF)|GPIO_phase3[current_motor_phase[i]]);
			         break;
			default: USART_transmit("Internal Error@motor_rotate Occurred!\r");
    			     break;
		}    
        
        if(t[i]<step[i])
          ctrl[i]=1;
        else
          ctrl[i]=0;
        test|=ctrl[i];  
        t[i]++;
      }
    }
    motor_delay();
  }
  motor_delay();
}

void motor_stop()
{
  GPIOB_BSRR=0xFF000000;
  GPIOC_BSRR=0xF0000000;
}
void USART_transmit(char* a)
{
  int i=0;
  while(a[i]!='\0')
  {
    while((USART_SR&0x0080)==0); /*wait until TXE==1*/
    USART_DR=a[i];
    i++;
  }
}
void Record_calibration(int motor_num)
{
/*  
  init_ADC1();
  ADC_fetch3(calibration_data);
  calibration_diff[motor_num]=calibration_data[motor_num][2]-calibration_data[motor_num][0];
*/
  current_step[motor_num]=0;
  return;
}

char flip_direction(char direction)
{
  if(direction=='A')
      direction='C';
  else
    direction='A';
  return direction;
}
void tracing_mode3()
{
  int ctrl_step[MOTOR_NUM];
  char ctrl_direction[MOTOR_NUM],last_direction[MOTOR_NUM];
  int m,step_R[MOTOR_NUM],counter[MOTOR_NUM],quit=0;
  unsigned int R[MOTOR_NUM][3],threshold=60,last_middle[MOTOR_NUM];
  status_int=1;
  init_ADC1();
  for(m=0;m<MOTOR_NUM;m++)
  {
        /*current_step[m]=0;*/
  	last_direction[m]='C';
  	last_middle[m]=0;
  	step_R[m]=0;
  	counter[m]=0;
  }
  while(1)
  {
    for(m=0;m<MOTOR_NUM;m++)
    {
      if(absolute(current_step[m]>=2147483640))
        quit=1;
    } /*restrict the largest number of steps*/
    if(ctrl_int==0||quit)
      return;
    else
    {
      ADC_fetch3(R);
      for(m=0;m<MOTOR_NUM;m++)
      {
        R[m][1]-=calibration_diff[m];
        if(absolute(R[m][1]-R[m][0])>threshold)
        {
          step_R[m]=0;
          counter[m]=0;
          ctrl_step[m]=STD_STEP;      
          if(R[m][1]>R[m][0])
            ctrl_direction[m]='C';
          else
            ctrl_direction[m]='A';
        }
        else
        {
          if(step_R[m]==0)
            counter[m]++;
          if(counter[m]>=3)
          {
              step_R[m]=2; /*all value greater than 1 is OK,just to prevent counter from increasing infinitely*/
              ctrl_step[m]=0;
              motor_delay();
          }
          else
          {
            ctrl_step[m]=QUARTER_STDSTEP;
            if(R[m][2]>last_middle[m])
              ctrl_direction[m]=flip_direction(last_direction[m]);
            else
              ctrl_direction[m]=last_direction[m];
            if(ctrl_direction[m]=='C')
              step_R[m]-=1;
            else
              step_R[m]+=1;
          }
        }
        last_middle[m]=R[m][1];
        last_direction[m]=ctrl_direction[m];
      }
      for(m=0;m<MOTOR_NUM;m++)
      {
      	if((current_step[m]>5000&&ctrl_direction[m]=='C')||(current_step[m]<-520&&ctrl_direction[m]=='A'))
          ctrl_step[m]=0;
      }
      USART_CR1&=0xFFFFFFDF; /*disable RXNE interrupt*/
      motor_rotate3(ctrl_direction,ctrl_step);
      record_step(ctrl_direction,ctrl_step);
      USART_CR1|=0x0020; /*re-enable RXNE interrupt*/
    }    
  }
}
void main()
{
  init_sysclk();
  init_GPIO();
  set_timer2();
  init_USART1();
  USART_transmit("USART Ready!\n\r");
  motor_stop();
  while(1)
  {
    status_int=0;
    motor_stop();
    if(ctrl_int==1)
    {
      tracing_mode3();
      ctrl_int=0;
    }
  }
}
